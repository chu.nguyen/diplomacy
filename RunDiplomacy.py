#!/usr/bin/env python3

# ------------------------------
# projects/Diplomacy/RunDiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# ------------------------------

# -------
# imports
# -------

import sys

from Diplomacy import diplomacy_solve

# ----
# main
# ----

if __name__ == "__main__":
    diplomacy_solve(sys.stdin, sys.stdout)

""" #pragma: no cover
% cat RunDiplomacy.in
1 10
100 200
201 210
900 1000



% python RunCollatz.py < RunCollatz.in > RunCollatz.out
(on Windows PowerShell, the command is as follows:
"c:\> Get-Content RunCollatz.in | py RunCollatz.py > RunCollatz.out"
)


% cat RunCollatz.out
1 10 1
100 200 1
201 210 1
900 1000 1



% pydoc3 -w Collatz
(Note: on Windows PowerShell, the command is as follows:
"c:\> python -m pydoc -w Collatz"
)
# That creates the file Collatz.html
"""
