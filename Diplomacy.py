#!/usr/bin/env python3

# ---------------------------
# projects/diplomacy/Diplomacy.py
# Copyright (C) 2019
# ---------------------------


# -------------
# diplomacy_print
# -------------


def diplomacy_print(w, army, position, dead):
    """
    print the result
    w a writer
    army: the list of army
    position: the list of final position of each army (either [dead], new city or old city)
    dead: the list of armies' status
    """
    for i in range(len(army)-1):
        for j in range(i, len(army)):
            if (army[i] > army[j]):
                army[i],army[j] = army[j], army[i]
                position[i], position[j] = position[j], position[j]
    for i in range(len(army)):
        w.write(str(army[i]) + " " + str(position[i]) + "\n")
    #for i in range(len(army)):
    #    if dead[i] == False:
    #        w.write(str(army[i]) + " " + str(position[i]))
    #    else:
    #        w.write(str(army[i]) + " " + "[dead]")
    #    w.write("\n")


# -------------
# diplomacy_check
# -------------

class Diplomacy(object):
    def __init__(self):
        self.armies = []  # name
        self.cities = []
        self.actions = []
        self.dead = []  # boolean
        self.position = []  # all the cities
        self.check = []

    def diplomacy_read(self, file):
        '''
        read the input file
        input: file -> the input file
        output: update the info of the class itself based on the input
        '''
        s = file.readline()
        while (s):
            tokens = s.split()
            assert(tokens[0] >= 'A' and tokens[0] <= 'Z')
            assert(tokens[1][0] >= 'A' and tokens[1][0] <= 'Z')
            assert(tokens[2] == "Hold" or tokens[2] ==
                   "Support" or tokens[2] == "Move")
            self.armies.append(tokens[0])
            self.cities.append(tokens[1])
            if (tokens[2] == "Hold"):
                self.actions.append((tokens[2], ""))
            else:
                self.actions.append((tokens[2], tokens[3]))
            s = file.readline()

        for i in range(len(self.armies)):
            self.dead.append(False)
            self.check.append(False)
            self.position.append(self.cities[i])

    def diplomacy_move(self, army, city):
        '''
        This function will trigger when an army moves to another city.
        It will determine the result of that army and all related armies
        (including the army holding that city and its supports)
        input: army -> the index of the army that makes the inital move
                        city -> the index of the city that the given army moves to
        output: update the status of related armies (dead or alive) and new position if move successfully
        '''
        # determine if there is a conflict
        # this is a list of index cities which clash at the given [city]
        conflicted_armies = []

        for i in range(len(self.armies)):
            if (self.dead[i] == False and self.check[i] == False and ((self.cities[i] == city and self.actions[i][0] != "Move") or self.actions[i][1] == city)):
                conflicted_armies.append(i)
                self.check[i] = True

        # if no conflict happens, update the position of the army
        if (len(conflicted_armies) <= 1):
            self.position[army] = city
            return

        # find the number of support
        number_of_support = []
        for i in range(len(conflicted_armies)):
            number_of_support.append(
                self.diplomacy_support(conflicted_armies[i]))

        # determine the survivor
        min_allies = 0  # temporary index
        max_allies = 0  # temporary index
        count = 0
        for i in range(1, len(conflicted_armies)):
            if (number_of_support[i] < number_of_support[min_allies]):
                min_allies = i
            if (number_of_support[i] > number_of_support[max_allies]):
                max_allies = i
                count = 0
            elif (number_of_support[i] == number_of_support[max_allies]):
                count = count + 1

        # comparison
        # count = 0 => 1 army has the largest number of support
        if (count == 0):
            for i in range(len(conflicted_armies)):
                if (i != max_allies):
                    self.dead[conflicted_armies[i]] = True
                    self.position[conflicted_armies[i]] = "[dead]"
                else:
                    self.position[conflicted_armies[max_allies]] = city
        # otherwise, all die
        else:
            for i in range(len(conflicted_armies)):
                self.dead[conflicted_armies[i]] = True
                self.position[conflicted_armies[i]] = "[dead]"

    def diplomacy_support(self, army):
        """
        This function is used to find the number of support an army has
        input: army -> the index of the army we need to calculate the number of support
        output: result -> the number of support that army has
        """
        result = 0
        for i in range(len(self.armies)):
            if (self.dead[i] == False and self.check[i] == False and self.actions[i][0] == "Support" and self.actions[i][1] == self.armies[army]):
                for j in range(len(self.armies)):
                    if (self.dead[j] == False and self.actions[j][0] == "Move" and self.actions[j][1] == self.cities[i]):
                        self.diplomacy_move(j, self.cities[i])
                if (self.dead[i] == False):
                    result = result + 1
        return result

# -------------
# diplomacy_solve
# -------------


def diplomacy_solve(r, w):
    """
    r a reader
    w a writer
    """
    a = Diplomacy()
    a.diplomacy_read(r)
    for i in range(len(a.armies)):
        if (a.dead[i] == False and a.actions[i][0] == "Move"):
            a.diplomacy_move(i, a.actions[i][1])
    diplomacy_print(w, a.armies, a.position, a.dead)
